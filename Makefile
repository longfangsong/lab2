all: *.erl lib/*.erl
	make clean
	make -C lib
	erl -compile *.erl lib/*.erl

clean:
	rm -f *.beam *.dump

test: all
	erl -noshell -eval "eunit:test(test_client), halt()"

run_server: all
	erl -sname server -eval "cchat:server()."

run_client: all
	erl -sname client_$RANDOM -remsh server@$(hostname -s)
	# cchat:client_tui().
	# /join #foo