-module(server).
-import(lists, [foreach/2]).
-export([start/1, stop/1, handle/2]).

% This record defines the structure of the state of a server.
-record(server_st, {
    channels, % [ChannelName]
    nicknames % {ClientPid => ClientNick}
}).

initial_state() ->
    #server_st{
        channels = [],
        nicknames = #{}
    }.

do_join(St, ChannelName, FromPid) ->
    Response = catch(genserver:request(list_to_atom(ChannelName), {join, FromPid})),
    case Response of
        {error, user_already_joined, _} -> {reply, {error, user_already_joined, "User already joined"},           St};
        {'EXIT', _} -> {reply, {error, channel_does_not_respond, "Channel does not respond"}, St};
        ok -> {reply, ok, St}
    end.

create_or_update_nick(Nicknames, Pid, NewNickname) ->
    case lists:member(NewNickname, maps:values(Nicknames)) of
        true ->
            nick_taken;
        false ->
            case Nicknames of
                #{ Pid := _ } ->
                    Nicknames#{ Pid := NewNickname };
                _ ->
                    Nicknames#{ Pid => NewNickname }
            end
    end.

handle(#server_st{channels = Channels, nicknames = Nicknames}, {join, FromPid, Nick, ChannelName}) ->
    io:format("~p join ~p ~n", [{FromPid, Nick}, ChannelName]),
    NewNicknames = case Nicknames of
        #{ FromPid := _ } ->
            Nicknames#{ FromPid := Nick };
        _ ->
            Nicknames#{ FromPid => Nick }
    end,
    St = #server_st{channels = Channels, nicknames = NewNicknames},
    case lists:filter(fun (Ch) when Ch == ChannelName -> true; (_) -> false end, Channels) of
        [ChannelName] -> do_join(St, ChannelName, FromPid);
        _ ->
            io:format("Trying to start channel~n", []),
            channel:start(ChannelName),
            io:format("Channel started~n", []),
            NewChannels = [ChannelName | Channels],
            NewSt = St#server_st{channels = NewChannels},
            do_join(NewSt, ChannelName, FromPid)
    end;

handle(St, {nick, FromPid, NewNick}) ->
    io:format("~p want to change nickname to ~p ~n", [FromPid, NewNick]),
    Nicknames = St#server_st.nicknames,
    case create_or_update_nick(Nicknames, FromPid, NewNick) of
        nick_taken -> {reply, {error, nick_taken, "Nick already taken"}, St};
        NewNicknames ->
            NewSt = St#server_st{nicknames = NewNicknames},
            {reply, ok, NewSt}
    end;

handle(St, stop) ->
    % TODO: move spawn into foreach, or is there any useful helper function in genserver?
    lists:foreach(fun(Ch) -> genserver:stop(list_to_atom(Ch)) end, St#server_st.channels),
    {reply, ok, St};

handle(St, R) ->
    io:format("Unexpected ~p~n    In state ~p~n", [R, St]).

start(ServerAtom) ->
    io:format("Starting~n", []),
    Pid = genserver:start(ServerAtom, initial_state(), fun server:handle/2),
    io:format("Registered~n", []),
    Pid.

% Stop the server process registered to the given name,
% together with any other associated processes
stop(ServerAtom) ->
    genserver:request(ServerAtom, stop),
    genserver:stop(ServerAtom),
    ok.
