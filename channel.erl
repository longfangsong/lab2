-module(channel).
-import(lists, [foreach/2]).
-export([start/1, stop/1, handle/2, initial_state/1]).

-record(channel_st, {
    name,
    members % [ClientPid]
}).

initial_state(Name) ->
    #channel_st{
        name = Name,
        members = []
    }.

handle(St, {join, Pid}) ->
    Members = St#channel_st.members,
    case lists:member(Pid, Members) of
        false ->
            NewMembers = [Pid | Members],
            NewSt = St#channel_st{members = NewMembers},
            {reply, ok, NewSt};
        true ->
            io:format("rejoin, ignore~n", []),
            {reply, {error, user_already_joined, "User has already been in the channel"}, St}
    end;
handle(St, {leave, Pid}) ->
    Members = St#channel_st.members,
    case lists:member(Pid, Members) of
        false ->
            io:format("not joined, ignore ~n", []),
            {reply, {error, user_not_joined, "User is not in the channel"}, St};
        true ->
            NewMembers = Members -- [Pid],
            NewSt = St#channel_st{members = NewMembers},
            {reply, ok, NewSt}
    end;
handle(St, {message_send, SenderPid, SenderNick, Msg}) ->
    Members = St#channel_st.members,
    case lists:member(SenderPid, Members) of
        true ->
            % TODO: move spawn into foreach, or is there any useful helper function in genserver?
            spawn(fun () -> foreach(
                fun
                    (Pid) when Pid /= SenderPid -> 
                        io:format("broadcast to ~p ~n", [Pid]), 
                        genserver:request(Pid, {message_receive, St#channel_st.name, SenderNick, Msg});
                    (_) ->
                        io:format("no need to broadcast to the sender~n")
                end, 
                Members)
            end),
            {reply, ok, St};
        false ->
            {reply, {error, user_not_joined, "User is not joined in channel"}, St}
    end;
handle(St, R) -> 
    io:format("Unexpected ~p~n    In state ~p~n", [R, St]).
start(Name) ->
    io:format("Starting Channel ~p~n", [Name]),
    Pid = genserver:start(list_to_atom(Name), initial_state(Name), fun channel:handle/2),
    Pid.
stop(ChannelAtom) ->
    io:format("Stopping Channel ~p~n", [ChannelAtom]),
    genserver:stop(ChannelAtom).
    